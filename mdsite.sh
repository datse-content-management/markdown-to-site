#!/bin/bash


function show_help() {
echo -e "mdsite --- Documentation"
echo
echo -e "options:"
echo
echo -e "Option \t\t\t\t\t\t Content"
echo -e "\t--configfile --configfile= -c -c= \t\t Configuration Filename"
echo -e "\t-v \t\t\t\t\t Increase Verbosity can be used multiple times eg. -v -v"
echo -e "\t--help -h /? \t\t\t\t Get Help"
echo
echo -e "Note:"
echo 
echo -e "Single letter options must be separated -vv does not work, as often does."
}

# Set our own defualt options
configfile="site.conf"
verbose=0

POSITIONAL=()
while [[ $# -gt 0 ]]
do
key="$1"
    case $key in
        -h|-\?|--help)
            show_help    # Display a usage synopsis.
            exit
            ;;
        -c)       # Takes an option argument; ensure it has been specified.
            if [ "$2" ]; then
                configfile=$2
                shift
            else
                die 'ERROR: "-c" requires a non-empty option argument.'
            fi
            ;;
        --configfile)       # Takes an option argument; ensure it has been specified.
            if [ "$2" ]; then
                configfile=$2
                shift
            else
                die 'ERROR: "--inputfile" requires a non-empty option argument.'
            fi
            ;;
        -c=?*|--configfile=?*)
            configfile=${1#*=} # Delete everything up to "=" and assign the remainder.
            ;;
        --configfile=)         # Handle the case of an empty --file=
            die 'ERROR: "--inputfile" requires a non-empty option argument.'
            ;;
        -c=)         # Handle the case of an empty --file=
            die 'ERROR: "-i" requires a non-empty option argument.'
            ;;
        -v|--verbose)
            verbose=$((verbose + 1))  # Each -v adds 1 to verbosity.
            ;;
        --)              # End of all options.
            shift
            break
            ;;
        -?*)
            printf 'WARN: Unknown option (ignored): %s\n' "$1" >&2
            ;;
        *)               # Default case: No more options, so break out of the loop.
            break
    esac

    shift
done

IFS="="
while read -r source dest
do
  if [[ ${source} == "" ]] 
  then 
    echo "Blank source: May be blank line in file?"
  else
    mddir.sh -i ${source} -o ${dest}
  fi
done < ${configfile}

