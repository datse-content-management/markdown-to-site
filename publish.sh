#!/bin/bash


function show_help() {
echo -e "publish --- Documentation"
echo
echo -e "options:"
echo
echo -e "Option \t\t\t\t\t\t Content"
echo -e "\t--configfile --configfile= -c -c= \t\t Configuration Filename"
echo -e "\t-v \t\t\t\t\t Increase Verbosity can be used multiple times eg. -v -v"
echo -e "\t--help -h /? \t\t\t\t Get Help"
echo
echo -e "Note:"
echo 
echo -e "Single letter options must be separated -vv does not work, as often does."
}

# Set our own defualt options
configfile="publish.conf"
verbose=0

POSITIONAL=()
while [[ $# -gt 0 ]]
do
key="$1"
    case $key in
        -h|-\?|--help)
            show_help    # Display a usage synopsis.
            exit
            ;;
        -c)       # Takes an option argument; ensure it has been specified.
            if [ "$2" ]; then
                configfile=$2
                shift
            else
                die 'ERROR: "-c" requires a non-empty option argument.'
            fi
            ;;
        --configfile)       # Takes an option argument; ensure it has been specified.
            if [ "$2" ]; then
                configfile=$2
                shift
            else
                die 'ERROR: "--inputfile" requires a non-empty option argument.'
            fi
            ;;
        -c=?*|--configfile=?*)
            configfile=${1#*=} # Delete everything up to "=" and assign the remainder.
            ;;
        --configfile=)         # Handle the case of an empty --file=
            die 'ERROR: "--inputfile" requires a non-empty option argument.'
            ;;
        -v|--verbose)
            verbose=$((verbose + 1))  # Each -v adds 1 to verbosity.
            ;;
        --)              # End of all options.
            shift
            break
            ;;
        -?*)
            printf 'WARN: Unknown option (ignored): %s\n' "$1" >&2
            ;;
        *)               # Default case: No more options, so break out of the loop.
            break
    esac

    shift
done

# jq '.site' $configfile # can be commented in to allow diagnostics if things don't work.
mdsourcedir=`jq '.site."mdsite-sourcedir"' $configfile | tr -d \"`
mdsiteconf=`jq '.site."mdsite-site-conf"' $configfile | tr -d \"`
mdsitedest=`jq '.site."mdsite-destdir"' $configfile | tr -d \"` 

echo "mdsite dir: $mdsourcedir"
echo "mdsite config:  $mdsiteconf"
echo "mdsite dest: $mdsitedest"

# This runs the mdsite script
# This is a bit more of a mess than I'd like
pushd $mdsourcedir
mdsite.sh -c $mdsiteconf
popd

echo "localsyncdirs"
# jq '.site.localsyncdirs[1]' $configfile # Can be uncommented for diagnostics
sourcedircount=`jq '.site.localsyncdirs | length' $configfile`

counter=0
while [ $counter -lt $sourcedircount ]
do
  echo $counter
  sourcedir=`jq ".site.localsyncdirs[$counter].source" $configfile | tr -d \"`
  destdir=`jq ".site.localsyncdirs[$counter].dest" $configfile | tr -d \"`
  echo "rsync $sourcedir $destdir"
  rsync $sourcedir $destdir
  ((counter++))
done

remotesource=`jq '.site.remotesync.localsyncdir' $configfile | tr -d \"`
remotedest=`jq '.site.remotesync."remotesync-rsync"' $configfile | tr -d \"`

echo "rsync -rvz $remotesource $remotedest"
rsync -rvz $remotesource $remotedest

