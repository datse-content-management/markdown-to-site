#!/bin/bash

function show_help() {
echo -e "md2html --- Documentation"
echo
echo -e "options:"
echo
echo -e "Option \t\t\t\t\t\t Content"
echo -e "\t--inputfile --inputfile= -i -i= \t Input Filename"
echo -e "\t--outputfile --outputfile= -o -o= \t Output Filename"
echo -e "\t--headerfile --headerfile= -h= \t\t Header Filename"
echo -e "\t--footerfile --footerfile= -f -f= \t Footer Filename"
echo -e "\t-v \t\t\t\t\t Increase Verbosity can be used multiple times eg. -v -v"
echo -e "\t--help -h /? \t\t\t\t Get Help"
echo
echo -e "Note:"
echo 
echo -e "Due to the use of -h as a stand alone, -h short option doesn't work as an way to add the header file without using -h="
echo 
echo -e "Single letter options must be separated -vv does not work, as often does."
}

die() { echo "$*" 1>&2 ; exit 1; }

# Set our own defualt options
inputfile=""
outputfile=""
headerfile="header.html"
footerfile="footer.html"
verbose=0

POSITIONAL=()
while [[ $# -gt 0 ]]
do
key="$1"
    case $key in
        -h|-\?|--help)
            show_help    # Display a usage synopsis.
            exit
            ;;
        -i)       # Takes an option argument; ensure it has been specified.
            if [ "$2" ]; then
                inputfile=$2
                shift
            else
                die 'ERROR: "-i" requires a non-empty option argument.'
            fi
            ;;
        --inputfile)       # Takes an option argument; ensure it has been specified.
            if [ "$2" ]; then
                inputfile=$2
                shift
            else
                die 'ERROR: "--inputfile" requires a non-empty option argument.'
            fi
            ;;
        -i=?*|--inputfile=?*)
            inputfile=${1#*=} # Delete everything up to "=" and assign the remainder.
            ;;
        --inputfile=)         # Handle the case of an empty --file=
            die 'ERROR: "--inputfile" requires a non-empty option argument.'
            ;;
        -i=)         # Handle the case of an empty --file=
            die 'ERROR: "-i" requires a non-empty option argument.'
            ;;
        -o)       # Takes an option argument; ensure it has been specified.
            if [ "$2" ]; then
                outputfile=$2
                shift
            else
                die 'ERROR: "-o" requires a non-empty option argument.'
            fi
            ;;
        --outputfile)       # Takes an option argument; ensure it has been specified.
            if [ "$2" ]; then
                outputfile=$2
                shift
            else
                die 'ERROR: "--outputfile" requires a non-empty option argument.'
            fi
            ;;
        -o=?*|--outputfile=?*)
            outputfile=${1#*=} # Delete everything up to "=" and assign the remainder.
            ;;
        --outputfile=)         # Handle the case of an empty --file=
            die 'ERROR: "--outputfile" requires a non-empty option argument.'
            ;;
        -o=)         # Handle the case of an empty --file=
            die 'ERROR: "-o" requires a non-empty option argument.'
            ;;
        -h)       # Takes an option argument; ensure it has been specified.
            if [ "$2" ]; then
                headerfile=$2
                shift
            else
                die 'ERROR: "-h" requires a non-empty option argument.'
            fi
            ;;
        --headerfile)       # Takes an option argument; ensure it has been specified.
            if [ "$2" ]; then
                headerfile=$2
                shift
            else
                die 'ERROR: "--headerfile" requires a non-empty option argument.'
            fi
            ;;
        -h=?*|--headerfile=?*)
            headerfile=${1#*=} # Delete everything up to "=" and assign the remainder.
            ;;
        --headerfile=)         # Handle the case of an empty --file=
            die 'ERROR: "--headerfile" requires a non-empty option argument.'
            ;;
        -h=)         # Handle the case of an empty --file=
            die 'ERROR: "-h" requires a non-empty option argument.'
            ;;
        -f)       # Takes an option argument; ensure it has been specified.
            if [ "$2" ]; then
                footerfile=$2
                shift
            else
                die 'ERROR: "-f" requires a non-empty option argument.'
            fi
            ;;
        --footerfile)       # Takes an option argument; ensure it has been specified.
            if [ "$2" ]; then
                footerfile=$2
                shift
            else
                die 'ERROR: "--footerfile" requires a non-empty option argument.'
            fi
            ;;
        -f=?*|--footerfile=?*)
            footerfile=${1#*=} # Delete everything up to "=" and assign the remainder.
            ;;
        --footerfile=)         # Handle the case of an empty --file=
            die 'ERROR: "--footerfile" requires a non-empty option argument.'
            ;;
        -f=)         # Handle the case of an empty --file=
            die 'ERROR: "-f" requires a non-empty option argument.'
            ;;
        -v|--verbose)
            verbose=$((verbose + 1))  # Each -v adds 1 to verbosity.
            ;;
        --)              # End of all options.
            shift
            break
            ;;
        -?*)
            printf 'WARN: Unknown option (ignored): %s\n' "$1" >&2
            ;;
        *)               # Default case: No more options, so break out of the loop.
            break
    esac

    shift
done

set -- "${POSITIONAL[@]}" # restore positional parameters

echo -e "Input File: \t ${inputfile}"
echo -e "Output File: \t ${outputfile}"
echo -e "Header File: \t ${headerfile}"
echo -e "Footer File: \t ${footerfile}"
echo -e "Verbosity: \t ${verbose}"

cat ${headerfile} > ${outputfile}
markdown2 -v --use-file-vars=markdown-extras $inputfile >> ${outputfile}
cat ${footerfile} >> ${outputfile}
