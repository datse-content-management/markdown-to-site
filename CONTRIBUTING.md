To contribute to this project contact us to see what you can provide.

This is being created initially as a simple tool which will allow the 
converstion of markdown source to a static HTML site.  The intention is
to provide an option to write markdown, and create a working site.

With source management such as provided with GitLab and GitHub the markdown
and the created site, should work essentially the same way.  

We do *not* expect that people will contribute in any way.  But stranger things
have happened.  