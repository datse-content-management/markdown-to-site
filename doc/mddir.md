<!-- 
   Local Variables:
   markdown-extras: tables
   End: 
   -->

# mddir --- Documentation

options:

|Option|Content|
|------|-------|
|--inputdir --inputdir= -i -i=|Input Dir|
|--outputdir --outputdir= -o -o=|Output Dir|
|--templatedir --templatedir= -o -o=|Template Dir|
|--headerfile --headerfile= -h=|Header Filename|
|--footerfile --footerfile= -f -f=|Footer Filename|
|-v|Increase Verbosity can be used multiple times eg. -v -v|
|--help -h /?|Get Help|

Note: 

Due to the use of -h as a stand alone, -h short option doesn't work as
an way to add the header file without using -h=

Single letter options must be separated -vv does not work, as often does.

Input dir and output dir default to current directory.

Template dir defaults to input directory if not set.

Headerfile and footerfile have defaults:

- headerfile=header.html
- footerfile=footer.html
