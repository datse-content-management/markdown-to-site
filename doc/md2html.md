<!-- 
   Local Variables:
   markdown-extras: tables
   End: 
   -->

# md2html --- Documentation

options:

|Option|Content|
|------|-------|
|--inputfile --inputfile= -i -i=|Input Filename|
|--outputfile --outputfile= -o -o=|Output Filename|
|--headerfile --headerfile= -h=|Header Filename|
|--footerfile --footerfile= -f -f=|Footer Filename|
|-v|Increase Verbosity can be used multiple times eg. -v -v|
|--help -h /?|Get Help|

Note: 

Due to the use of -h as a stand alone, -h short option doesn't work as
an way to add the header file without using -h=

Single letter options must be separated -vv does not work, as often does.

inputfile and outputfile need to be set to work.

Headerfile and footerfile have defaults:

- headerfile=header.html
- footerfile=footer.html
