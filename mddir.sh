#!/bin/bash

function show_help() {
echo -e "mddir --- Documentation"
echo
echo -e "options:"
echo
echo -e "Option \t\t\t\t\t\t Content"
echo -e "\t--inputdir --inputdir= -i -i= \t\t Input Filename"
echo -e "\t--outputdir --outputdir= -o -o= \t Output Filename"
echo -e "\t--templatedir --templatedir= -t -t= \t Template Dir"
echo -e "\t--headerfile --headerfile= -h= \t\t Header Filename"
echo -e "\t--footerfile --footerfile= -f -f= \t Footer Filename"
echo -e "\t-v \t\t\t\t\t Increase Verbosity can be used multiple times eg. -v -v"
echo -e "\t--help -h /? \t\t\t\t Get Help"
echo
echo -e "Note:"
echo 
echo -e "Due to the use of -h as a stand alone, -h short option doesn't work as an way to add the header file without using -h="
echo 
echo -e "Single letter options must be separated -vv does not work, as often does."
echo 
echo -e "Input and output dirs default to current directory"
echo 
echo -e "Template Dir, if not set, defaults to input dir"
}

# Set our own defualt options
inputdir="."
outputdir=""
templatedir=""
headerfile="header.html"
footerfile="footer.html"
verbose=0

POSITIONAL=()
while [[ $# -gt 0 ]]
do
key="$1"
    case $key in
        -h|-\?|--help)
            show_help    # Display a usage synopsis.
            exit
            ;;
        -i)       # Takes an option argument; ensure it has been specified.
            if [ "$2" ]; then
                inputdir=$2
                shift
            else
                die 'ERROR: "-i" requires a non-empty option argument.'
            fi
            ;;
        --inputdir)       # Takes an option argument; ensure it has been specified.
            if [ "$2" ]; then
                inputdir=$2
                shift
            else
                die 'ERROR: "--inputfile" requires a non-empty option argument.'
            fi
            ;;
        -i=?*|--inputdir=?*)
            inputdir=${1#*=} # Delete everything up to "=" and assign the remainder.
            ;;
        --inputdir=)         # Handle the case of an empty --file=
            die 'ERROR: "--inputfile" requires a non-empty option argument.'
            ;;
        -i=)         # Handle the case of an empty --file=
            die 'ERROR: "-i" requires a non-empty option argument.'
            ;;
        -o)       # Takes an option argument; ensure it has been specified.
            if [ "$2" ]; then
                outputdir=$2
                shift
            else
                die 'ERROR: "-o" requires a non-empty option argument.'
            fi
            ;;
        --outputdir)       # Takes an option argument; ensure it has been specified.
            if [ "$2" ]; then
                outputdir=$2
                shift
            else
                die 'ERROR: "--outputfile" requires a non-empty option argument.'
            fi
            ;;
        -o=?*|--outputdir=?*)
            outputdir=${1#*=} # Delete everything up to "=" and assign the remainder.
            ;;
        --outputdir=)         # Handle the case of an empty --file=
            die 'ERROR: "--outputfile" requires a non-empty option argument.'
            ;;
        -o=)         # Handle the case of an empty --file=
            die 'ERROR: "-o" requires a non-empty option argument.'
            ;;
        -t)       # Takes an option argument; ensure it has been specified.
            if [ "$2" ]; then
                templatedir=$2
                shift
            else
                die 'ERROR: "-o" requires a non-empty option argument.'
            fi
            ;;
        --templatedir)       # Takes an option argument; ensure it has been specified.
            if [ "$2" ]; then
                templatedir=$2
                shift
            else
                die 'ERROR: "--outputfile" requires a non-empty option argument.'
            fi
            ;;
        -t=?*|--templatedir=?*)
            templatedir=${1#*=} # Delete everything up to "=" and assign the remainder.
            ;;
        --templatedir=)         # Handle the case of an empty --file=
            die 'ERROR: "--outputfile" requires a non-empty option argument.'
            ;;
        -t=)         # Handle the case of an empty --file=
            die 'ERROR: "-o" requires a non-empty option argument.'
            ;;
        --headerfile)       # Takes an option argument; ensure it has been specified.
            if [ "$2" ]; then
                headerfile=$2
                shift
            else
                die 'ERROR: "--headerfile" requires a non-empty option argument.'
            fi
            ;;
        -h=?*|--headerfile=?*)
            headerfile=${1#*=} # Delete everything up to "=" and assign the remainder.
            ;;
        --headerfile=)         # Handle the case of an empty --file=
            die 'ERROR: "--headerfile" requires a non-empty option argument.'
            ;;
        -h=)         # Handle the case of an empty --file=
            die 'ERROR: "-h" requires a non-empty option argument.'
            ;;
        -f)       # Takes an option argument; ensure it has been specified.
            if [ "$2" ]; then
                footerfile=$2
                shift
            else
                die 'ERROR: "-f" requires a non-empty option argument.'
            fi
            ;;
        --footerfile)       # Takes an option argument; ensure it has been specified.
            if [ "$2" ]; then
                footerfile=$2
                shift
            else
                die 'ERROR: "--footerfile" requires a non-empty option argument.'
            fi
            ;;
        -f=?*|--footerfile=?*)
            footerfile=${1#*=} # Delete everything up to "=" and assign the remainder.
            ;;
        --footerfile=)         # Handle the case of an empty --file=
            die 'ERROR: "--footerfile" requires a non-empty option argument.'
            ;;
        -f=)         # Handle the case of an empty --file=
            die 'ERROR: "-f" requires a non-empty option argument.'
            ;;
        -v|--verbose)
            verbose=$((verbose + 1))  # Each -v adds 1 to verbosity.
            ;;
        --)              # End of all options.
            shift
            break
            ;;
        -?*)
            printf 'WARN: Unknown option (ignored): %s\n' "$1" >&2
            ;;
        *)               # Default case: No more options, so break out of the loop.
            break
    esac

    shift
done

if [[ ${templatedir} == "" ]] 
then
  templatedir="template"
fi

if [[ ${outputdir} == "" ]]
then
  outputdir="html"
fi

set -- "${POSITIONAL[@]}" # restore positional parameters

echo -e "Input Dir: \t ${inputdir}"
echo -e "Output Dir: \t ${outputdir}"
echo -e "Template Dir: \t ${templatedir}"
echo -e "Header File: \t ${headerfile}"
echo -e "Footer File: \t ${footerfile}"
echo -e "Verbosity: \t ${verbose}"

workingdir="${PWD}"
if cd ${inputdir} ; then
  for file in $(ls *.md) 
  do
    outputfile="${file%.md}.html"
    if [ "${file}" -nt "${outputdir}/${outputfile}" ]; then 
      echo "${file} updated"
      md2html.sh -i ${file} -o ${outputdir}/${outputfile} -h=${templatedir}/${headerfile} -f ${templatedir}/${footerfile}
    elif [ "${templatedir}/${headerfile}" -nt "${outputdir}/${outputfile}" ] ; then
      echo "Header Updated"
      md2html.sh -i ${file} -o ${outputdir}/${outputfile} -h=${templatedir}/${headerfile} -f ${templatedir}/${footerfile}
    elif [ "${templatedir}/${footerfile}" -nt "${outputdir}/${outputfile}" ] ; then
      echo "Footer Updated"
      md2html.sh -i ${file} -o ${outputdir}/${outputfile} -h=${templatedir}/${headerfile} -f ${templatedir}/${footerfile}
    else 
      echo "Dest ${outputdir}/${outputfile} is newer than source: ${inputdir}/${file}... skipping."
    fi
  done
else 
  echo -e "Source directory: \n\t${inputdir}\nis inaccessible."
  echo -e ""
  echo -e "Directories with spaces can cause problems."
fi

